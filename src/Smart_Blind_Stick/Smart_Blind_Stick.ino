
#include "Arduino.h"
#include "SoftwareSerial.h"
#include "DFRobotDFPlayerMini.h"
#include <Servo.h>
#include <NewPing.h>
// First Ultrasonic
#define TRIGGER_PIN  7        
#define ECHO_PIN     6       
#define MAX_DISTANCE 200      // Maximum distance we want to ping for (in centimeters
// Second Ultrasonic
#define TRIGGER_PIN2  13        
#define ECHO_PIN2     12       
#define MAX_DISTANCE2 200
 // NewPing setup of Ultrasonics' pins and maximum distance.
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); 
NewPing sonar2(TRIGGER_PIN2, ECHO_PIN2, MAX_DISTANCE2);
// create servo object to control a servo
Servo myservo;                
Servo myservo2;
int pos = 0;                  
SoftwareSerial mySoftwareSerial(10, 11); // RX, TX
// DFPlayer setup
DFRobotDFPlayerMini myDFPlayer;
void printDetail(uint8_t type, int value);
void setup()
{
  mySoftwareSerial.begin(9600);
  Serial.begin(115200);
  Serial.println();
  Serial.println(F("DFRobot DFPlayer Mini Demo"));
  Serial.println(F("Initializing DFPlayer ... (May take 3~5 seconds)"));
  
  if (!myDFPlayer.begin(mySoftwareSerial)) {  //Use softwareSerial to communicate with mp3.
    Serial.println(F("Unable to begin:"));
    Serial.println(F("1.Please recheck the connection!"));
    Serial.println(F("2.Please insert the SD card!"));
    while(true);
  }
  Serial.println(F("DFPlayer Mini online."));
  myDFPlayer.setTimeOut(500); //Set serial communictaion time out 500ms
  myDFPlayer.volume(30);  //Set volume value (0~30).
  myDFPlayer.EQ(DFPLAYER_EQ_NORMAL);
  myDFPlayer.outputDevice(DFPLAYER_DEVICE_SD);
  Serial.println(myDFPlayer.readState()); //read mp3 state
  Serial.println(myDFPlayer.readVolume()); //read current volume
  Serial.println(myDFPlayer.readEQ()); //read EQ setting
  Serial.println(myDFPlayer.readFileCounts()); //read all file counts in SD card
  Serial.println(myDFPlayer.readCurrentFileNumber()); //read current play file number
  Serial.println(myDFPlayer.readFileCountsInFolder(3)); //read fill counts in folder SD:/03
myservo.attach(9);  // attaches the servo on pin 9 to the servo object
myservo2.attach(5); // attaches the servo on pin 4 to the servo object
myservo.writeMicroseconds(1500); // midway point
myservo2.writeMicroseconds(1500);
delay(2000);   
}

void loop() {
sweepServo();
    }
 // Ultrasonic functioning
void pingSensor() 
{
  delay(10);                     
  int pingcm = sonar.ping_cm();
  Serial.print(pingcm); 
  Serial.println("cm");
  if ((pingcm < 60) && (pingcm != 0)) 
    { 
     if ((sonar.ping_cm() <= 60) && (sonar.ping_cm() != 0)) 
      {
        if (pos < 50) {
          myDFPlayer.play(3);  //Obstacle on your right            
                  delay(2000);
        } else if (pos > 120) {
          myDFPlayer.play(2);  //Obstacle on your left
                delay(2000);
        } else {
          myDFPlayer.play(1); //Obstacle Ahead
                  delay(2000);
        }
        }
    }  
}
void pingSensor2() 
{
  delay(10);                     
  int pingcm2 = sonar2.ping_cm();
  Serial.print(pingcm2); 
  Serial.println("cm");
  if ((pingcm2 < 60) && (pingcm2 != 0)) 
    { 
     if ((sonar2.ping_cm() <= 60) && (sonar2.ping_cm() != 0)) 
      {
        if (pos < 50) {
          myDFPlayer.play(3);  //Obstacle on your right
                  delay(2000);
        } else if (pos > 120) {
          myDFPlayer.play(2);  //Obstacle on your left
                delay(2000);
        } else {
          myDFPlayer.play(1); //Obstacle Ahead     
                  delay(2000);
        }
        }
    }  
}

void sweepServo()  {
  myservo.attach(9);
  myservo2.attach(5);
  for (pos = 0; pos <= 180; pos += 4) { // goes from 0 degrees to 180 degrees      
    Serial.print("Servo Position ");
    Serial.println(pos);
    pingSensor();
    pingSensor2();
    myservo.write(pos);
    myservo2.write(pos); 
    delay(10);                       // waits 10ms for the servo to reach the position
  }
  for (pos = 180; pos >= 0; pos -= 4) { // goes from 180 degrees to 0 degrees
    Serial.print("Servo Position ");
    Serial.println(pos);
    pingSensor();
    pingSensor2();
    myservo.write(pos);
    myservo2.write(pos); 
    delay(10);                       // waits 10ms for the servo to reach the position
  } 
}
