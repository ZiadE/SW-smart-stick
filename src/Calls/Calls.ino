#include <SoftwareSerial.h>
SoftwareSerial A_7(2, 3); // RX, TX
// Defining variables
int state = 0;
int state2 = 0;
int button = 7;
int button2 = 6;
int answer = 0;
void setup() {
  // A7 Module setup
  Serial.begin(115200);
  A_7.begin(115200);
  A_7.println("AT+IPR=115200");
  A_7.println("AT");
  
}

void loop() {
   state = digitalRead(button);
   state2 = digitalRead(button2);
   // Switch on to answer incoming call
   if (state == HIGH) {
  A_7.println("ATA");
  answer = 1;
  // Switch off to hang up the current call
   }
  
  // Press button to call the blind's guardian
  if (state2 == HIGH) {
     A_7.println("ATD01015162406");
    }
  // Prints the incoming data from A7 Module
  if (A_7.available()){
     Serial.write(A_7.read());
    }
  if (Serial.available()) {
    A_7.write(Serial.read());
    }
  
}
