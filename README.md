# SW-smart-stick

A hardware project. A stick where it navigates the blind.

## Project Overview

The project's goal is to warn the blind if there's an obstacle that could've distrupt him.
An extra feature to the project is an application that the blind's guardian can use to track the blind.

